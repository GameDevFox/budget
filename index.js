'strict mode';

const fs = require('fs');
const readline = require('readline');

const _ = require('lodash');
const moment = require('moment');
const numeral = require('numeral');
const csvParser = require('csv-parse/lib/sync');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const entry = [];

const parseTrans = t => ({
  desc: t.Description,
  category: t['Transaction Category'],
  date: t['Posting Date'],
  effectiveDate: t['Effective Date'],
  checkNumber: t['Check Number'],
  amount: t.Amount,
  balance: t.Balance
});

const transCSV = fs.readFileSync('ExportedTransactions.csv');
const trans = csvParser(transCSV, { columns: true })
  .reverse()
  .map(parseTrans);

const printTrans = (idx, t) => {
  const category = _.isEmpty(t.category) ? '' : `(${t.category}) `;

  const m = moment(t.effectiveDate);
  const d = m.format('ddd MMM Do')

  console.log(`=== ${idx+1}/${trans.length} ${d} ===`);
  console.log(category + t.desc);

  if(t.checkNumber)
    console.log('Check Number:', t.checkNumber)

  console.log(numeral(t.amount).format('$0,0.00'))
};

const processTrans = idx => {
  if(idx === trans.length) {
    console.log('Done!')
    return;
  }

  printTrans(idx, trans[idx]);

  rl.question('How about this? ', response => {
    console.log('RES', response);
    processTrans(idx+1);
  });
};

processTrans(0);
